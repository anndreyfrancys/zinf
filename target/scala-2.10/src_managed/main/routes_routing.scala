// @SOURCE:C:/Users/anndreyfrancys/Documents/Projetos/zinf/conf/routes
// @HASH:a2e0b5a123715401a49c6467f6b660c784409a61
// @DATE:Fri Nov 13 15:30:09 BRST 2015


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:6
private[this] lazy val controllers_Application_index0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:9
private[this] lazy val controllers_Application_login1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:10
private[this] lazy val controllers_Application_authenticate2 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:11
private[this] lazy val controllers_Application_logout3 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("logout"))))
        

// @LINE:12
private[this] lazy val controllers_Application_sobre4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("sobre"))))
        

// @LINE:13
private[this] lazy val controllers_Application_arquivos5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("arquivos"))))
        

// @LINE:14
private[this] lazy val controllers_Application_readGrupo6 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("grupo/"),DynamicPart("id", """[^/]+""",true),StaticPart("/view"))))
        

// @LINE:15
private[this] lazy val controllers_Application_meusgrupos7 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("meusgrupos"))))
        

// @LINE:16
private[this] lazy val controllers_Application_cadGrupo8 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("cadGrupo"))))
        

// @LINE:17
private[this] lazy val controllers_Application_addGrupo9 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("addGrupo"))))
        

// @LINE:18
private[this] lazy val controllers_Application_cadUser10 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("cadUser"))))
        

// @LINE:19
private[this] lazy val controllers_Application_deletaGrupo11 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("cadGrupo/"),DynamicPart("id", """[^/]+""",true),StaticPart("/del"))))
        

// @LINE:20
private[this] lazy val controllers_Application_editaGrupo12 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("cadGrupo/"),DynamicPart("id", """[^/]+""",true),StaticPart("/edit"))))
        

// @LINE:21
private[this] lazy val controllers_Application_updateGrupo13 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("cadGrupo/"),DynamicPart("id", """[^/]+""",true),StaticPart("/update"))))
        

// @LINE:24
private[this] lazy val controllers_Assets_at14 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.login()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.authenticate()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """logout""","""controllers.Application.logout()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """sobre""","""controllers.Application.sobre()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """arquivos""","""controllers.Application.arquivos()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """grupo/$id<[^/]+>/view""","""controllers.Application.readGrupo(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """meusgrupos""","""controllers.Application.meusgrupos()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """cadGrupo""","""controllers.Application.cadGrupo()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """addGrupo""","""controllers.Application.addGrupo()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """cadUser""","""controllers.Application.cadUser()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """cadGrupo/$id<[^/]+>/del""","""controllers.Application.deletaGrupo(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """cadGrupo/$id<[^/]+>/edit""","""controllers.Application.editaGrupo(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """cadGrupo/$id<[^/]+>/update""","""controllers.Application.updateGrupo(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:6
case controllers_Application_index0(params) => {
   call { 
        invokeHandler(controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Nil,"GET", """ Home page""", Routes.prefix + """"""))
   }
}
        

// @LINE:9
case controllers_Application_login1(params) => {
   call { 
        invokeHandler(controllers.Application.login(), HandlerDef(this, "controllers.Application", "login", Nil,"GET", """ Authentication""", Routes.prefix + """login"""))
   }
}
        

// @LINE:10
case controllers_Application_authenticate2(params) => {
   call { 
        invokeHandler(controllers.Application.authenticate(), HandlerDef(this, "controllers.Application", "authenticate", Nil,"POST", """""", Routes.prefix + """login"""))
   }
}
        

// @LINE:11
case controllers_Application_logout3(params) => {
   call { 
        invokeHandler(controllers.Application.logout(), HandlerDef(this, "controllers.Application", "logout", Nil,"GET", """""", Routes.prefix + """logout"""))
   }
}
        

// @LINE:12
case controllers_Application_sobre4(params) => {
   call { 
        invokeHandler(controllers.Application.sobre(), HandlerDef(this, "controllers.Application", "sobre", Nil,"GET", """""", Routes.prefix + """sobre"""))
   }
}
        

// @LINE:13
case controllers_Application_arquivos5(params) => {
   call { 
        invokeHandler(controllers.Application.arquivos(), HandlerDef(this, "controllers.Application", "arquivos", Nil,"GET", """""", Routes.prefix + """arquivos"""))
   }
}
        

// @LINE:14
case controllers_Application_readGrupo6(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Application.readGrupo(id), HandlerDef(this, "controllers.Application", "readGrupo", Seq(classOf[Long]),"GET", """""", Routes.prefix + """grupo/$id<[^/]+>/view"""))
   }
}
        

// @LINE:15
case controllers_Application_meusgrupos7(params) => {
   call { 
        invokeHandler(controllers.Application.meusgrupos(), HandlerDef(this, "controllers.Application", "meusgrupos", Nil,"GET", """""", Routes.prefix + """meusgrupos"""))
   }
}
        

// @LINE:16
case controllers_Application_cadGrupo8(params) => {
   call { 
        invokeHandler(controllers.Application.cadGrupo(), HandlerDef(this, "controllers.Application", "cadGrupo", Nil,"GET", """""", Routes.prefix + """cadGrupo"""))
   }
}
        

// @LINE:17
case controllers_Application_addGrupo9(params) => {
   call { 
        invokeHandler(controllers.Application.addGrupo(), HandlerDef(this, "controllers.Application", "addGrupo", Nil,"POST", """""", Routes.prefix + """addGrupo"""))
   }
}
        

// @LINE:18
case controllers_Application_cadUser10(params) => {
   call { 
        invokeHandler(controllers.Application.cadUser(), HandlerDef(this, "controllers.Application", "cadUser", Nil,"POST", """""", Routes.prefix + """cadUser"""))
   }
}
        

// @LINE:19
case controllers_Application_deletaGrupo11(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Application.deletaGrupo(id), HandlerDef(this, "controllers.Application", "deletaGrupo", Seq(classOf[Long]),"GET", """""", Routes.prefix + """cadGrupo/$id<[^/]+>/del"""))
   }
}
        

// @LINE:20
case controllers_Application_editaGrupo12(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Application.editaGrupo(id), HandlerDef(this, "controllers.Application", "editaGrupo", Seq(classOf[Long]),"GET", """""", Routes.prefix + """cadGrupo/$id<[^/]+>/edit"""))
   }
}
        

// @LINE:21
case controllers_Application_updateGrupo13(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Application.updateGrupo(id), HandlerDef(this, "controllers.Application", "updateGrupo", Seq(classOf[Long]),"GET", """""", Routes.prefix + """cadGrupo/$id<[^/]+>/update"""))
   }
}
        

// @LINE:24
case controllers_Assets_at14(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        
}

}
     