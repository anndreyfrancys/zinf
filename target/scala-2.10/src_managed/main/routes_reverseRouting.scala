// @SOURCE:C:/Users/anndreyfrancys/Documents/Projetos/zinf/conf/routes
// @HASH:a2e0b5a123715401a49c6467f6b660c784409a61
// @DATE:Fri Nov 13 15:30:09 BRST 2015

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString


// @LINE:24
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers {

// @LINE:24
class ReverseAssets {
    

// @LINE:24
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          

// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
class ReverseApplication {
    

// @LINE:19
def deletaGrupo(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "cadGrupo/" + implicitly[PathBindable[Long]].unbind("id", id) + "/del")
}
                                                

// @LINE:16
def cadGrupo(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "cadGrupo")
}
                                                

// @LINE:13
def arquivos(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "arquivos")
}
                                                

// @LINE:15
def meusgrupos(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "meusgrupos")
}
                                                

// @LINE:14
def readGrupo(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "grupo/" + implicitly[PathBindable[Long]].unbind("id", id) + "/view")
}
                                                

// @LINE:20
def editaGrupo(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "cadGrupo/" + implicitly[PathBindable[Long]].unbind("id", id) + "/edit")
}
                                                

// @LINE:11
def logout(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "logout")
}
                                                

// @LINE:21
def updateGrupo(id:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "cadGrupo/" + implicitly[PathBindable[Long]].unbind("id", id) + "/update")
}
                                                

// @LINE:12
def sobre(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "sobre")
}
                                                

// @LINE:10
def authenticate(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "login")
}
                                                

// @LINE:6
def index(): Call = {
   Call("GET", _prefix)
}
                                                

// @LINE:17
def addGrupo(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "addGrupo")
}
                                                

// @LINE:18
def cadUser(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "cadUser")
}
                                                

// @LINE:9
def login(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "login")
}
                                                
    
}
                          
}
                  


// @LINE:24
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers.javascript {

// @LINE:24
class ReverseAssets {
    

// @LINE:24
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              

// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
class ReverseApplication {
    

// @LINE:19
def deletaGrupo : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.deletaGrupo",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "cadGrupo/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/del"})
      }
   """
)
                        

// @LINE:16
def cadGrupo : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.cadGrupo",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "cadGrupo"})
      }
   """
)
                        

// @LINE:13
def arquivos : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.arquivos",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "arquivos"})
      }
   """
)
                        

// @LINE:15
def meusgrupos : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.meusgrupos",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "meusgrupos"})
      }
   """
)
                        

// @LINE:14
def readGrupo : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.readGrupo",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "grupo/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/view"})
      }
   """
)
                        

// @LINE:20
def editaGrupo : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.editaGrupo",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "cadGrupo/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/edit"})
      }
   """
)
                        

// @LINE:11
def logout : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.logout",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
      }
   """
)
                        

// @LINE:21
def updateGrupo : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.updateGrupo",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "cadGrupo/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/update"})
      }
   """
)
                        

// @LINE:12
def sobre : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.sobre",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "sobre"})
      }
   """
)
                        

// @LINE:10
def authenticate : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.authenticate",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        

// @LINE:6
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

// @LINE:17
def addGrupo : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.addGrupo",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addGrupo"})
      }
   """
)
                        

// @LINE:18
def cadUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.cadUser",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "cadUser"})
      }
   """
)
                        

// @LINE:9
def login : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.login",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        
    
}
              
}
        


// @LINE:24
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers.ref {


// @LINE:24
class ReverseAssets {
    

// @LINE:24
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          

// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
class ReverseApplication {
    

// @LINE:19
def deletaGrupo(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.deletaGrupo(id), HandlerDef(this, "controllers.Application", "deletaGrupo", Seq(classOf[Long]), "GET", """""", _prefix + """cadGrupo/$id<[^/]+>/del""")
)
                      

// @LINE:16
def cadGrupo(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.cadGrupo(), HandlerDef(this, "controllers.Application", "cadGrupo", Seq(), "GET", """""", _prefix + """cadGrupo""")
)
                      

// @LINE:13
def arquivos(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.arquivos(), HandlerDef(this, "controllers.Application", "arquivos", Seq(), "GET", """""", _prefix + """arquivos""")
)
                      

// @LINE:15
def meusgrupos(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.meusgrupos(), HandlerDef(this, "controllers.Application", "meusgrupos", Seq(), "GET", """""", _prefix + """meusgrupos""")
)
                      

// @LINE:14
def readGrupo(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.readGrupo(id), HandlerDef(this, "controllers.Application", "readGrupo", Seq(classOf[Long]), "GET", """""", _prefix + """grupo/$id<[^/]+>/view""")
)
                      

// @LINE:20
def editaGrupo(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.editaGrupo(id), HandlerDef(this, "controllers.Application", "editaGrupo", Seq(classOf[Long]), "GET", """""", _prefix + """cadGrupo/$id<[^/]+>/edit""")
)
                      

// @LINE:11
def logout(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.logout(), HandlerDef(this, "controllers.Application", "logout", Seq(), "GET", """""", _prefix + """logout""")
)
                      

// @LINE:21
def updateGrupo(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.updateGrupo(id), HandlerDef(this, "controllers.Application", "updateGrupo", Seq(classOf[Long]), "GET", """""", _prefix + """cadGrupo/$id<[^/]+>/update""")
)
                      

// @LINE:12
def sobre(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.sobre(), HandlerDef(this, "controllers.Application", "sobre", Seq(), "GET", """""", _prefix + """sobre""")
)
                      

// @LINE:10
def authenticate(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.authenticate(), HandlerDef(this, "controllers.Application", "authenticate", Seq(), "POST", """""", _prefix + """login""")
)
                      

// @LINE:6
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Seq(), "GET", """ Home page""", _prefix + """""")
)
                      

// @LINE:17
def addGrupo(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.addGrupo(), HandlerDef(this, "controllers.Application", "addGrupo", Seq(), "POST", """""", _prefix + """addGrupo""")
)
                      

// @LINE:18
def cadUser(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.cadUser(), HandlerDef(this, "controllers.Application", "cadUser", Seq(), "POST", """""", _prefix + """cadUser""")
)
                      

// @LINE:9
def login(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.login(), HandlerDef(this, "controllers.Application", "login", Seq(), "GET", """ Authentication""", _prefix + """login""")
)
                      
    
}
                          
}
        
    