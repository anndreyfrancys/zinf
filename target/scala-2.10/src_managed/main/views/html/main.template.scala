
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object main extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Html,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(title: String)(content: Html):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.32*/("""

<!DOCTYPE html>
<!-- TEMPLATE PADRAO --- APLICATIVOS BY ANDRE F. M. BATISTA -->
<html>
	<head>
		<!-- define o titulo da pagina -->
		<title>"""),_display_(Seq[Any](/*8.11*/title)),format.raw/*8.16*/("""</title>

		<link rel="apple-touch-icon" sizes="57x57" href=""""),_display_(Seq[Any](/*10.53*/routes/*10.59*/.Assets.at("images/favicon/apple-icon-57x57.png"))),format.raw/*10.108*/("""">
		<link rel="apple-touch-icon" sizes="60x60" href=""""),_display_(Seq[Any](/*11.53*/routes/*11.59*/.Assets.at("images/favicon/apple-icon-60x60.png"))),format.raw/*11.108*/("""">
		<link rel="apple-touch-icon" sizes="72x72" href=""""),_display_(Seq[Any](/*12.53*/routes/*12.59*/.Assets.at("images/favicon/apple-icon-72x72.png"))),format.raw/*12.108*/("""">
		<link rel="apple-touch-icon" sizes="76x76" href=""""),_display_(Seq[Any](/*13.53*/routes/*13.59*/.Assets.at("images/favicon/apple-icon-76x76.png"))),format.raw/*13.108*/("""">
		<link rel="apple-touch-icon" sizes="114x114" href=""""),_display_(Seq[Any](/*14.55*/routes/*14.61*/.Assets.at("images/favicon/apple-icon-114x114.png"))),format.raw/*14.112*/("""">
		<link rel="apple-touch-icon" sizes="120x120" href=""""),_display_(Seq[Any](/*15.55*/routes/*15.61*/.Assets.at("images/favicon/apple-icon-120x120.png"))),format.raw/*15.112*/("""">
		<link rel="apple-touch-icon" sizes="144x144" href=""""),_display_(Seq[Any](/*16.55*/routes/*16.61*/.Assets.at("images/favicon/apple-icon-144x144.png"))),format.raw/*16.112*/("""">
		<link rel="apple-touch-icon" sizes="152x152" href=""""),_display_(Seq[Any](/*17.55*/routes/*17.61*/.Assets.at("images/favicon/apple-icon-152x152.png"))),format.raw/*17.112*/("""">
		<link rel="apple-touch-icon" sizes="180x180" href=""""),_display_(Seq[Any](/*18.55*/routes/*18.61*/.Assets.at("images/favicon/apple-icon-180x180.png"))),format.raw/*18.112*/("""">
		<link rel="icon" type="image/png" sizes="192x192"  href=""""),_display_(Seq[Any](/*19.61*/routes/*19.67*/.Assets.at("images/favicon/android-icon-192x192.png"))),format.raw/*19.120*/("""">
		<link rel="icon" type="image/png" sizes="32x32" href=""""),_display_(Seq[Any](/*20.58*/routes/*20.64*/.Assets.at("images/favicon/favicon-32x32.png"))),format.raw/*20.110*/("""">
		<link rel="icon" type="image/png" sizes="96x96" href=""""),_display_(Seq[Any](/*21.58*/routes/*21.64*/.Assets.at("images/favicon/favicon-96x96.png"))),format.raw/*21.110*/("""">
		<link rel="icon" type="image/png" sizes="16x16" href=""""),_display_(Seq[Any](/*22.58*/routes/*22.64*/.Assets.at("images/favicon/favicon-16x16.png"))),format.raw/*22.110*/("""">
		<meta name="DC.Title" content="ZINF">
		<meta name="DC.Creator" content="Anndrey Francys">
		<meta name="DC.Subject" content="Grupo de estudo">
		<meta name="DC.Description" content="Portal dedicado a criação de grupos de trabalho.">
		<meta name="DC.Publisher" content="Anndrey Francys">
		<meta name="DC.Contributor" content="Anndrey Francys">
		<meta name="DC.Date" content="12/11/2015">
		<meta name="DC.Type" content="Text">
		<meta name="DC.Source" content="java">
		<meta name="DC.Language" content="pt-BR">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">	  
		<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*37.48*/routes/*37.54*/.Assets.at("stylesheets/main.css"))),format.raw/*37.88*/("""">
		<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*38.48*/routes/*38.54*/.Assets.at("stylesheets/style.css"))),format.raw/*38.89*/("""">
		<link href=""""),_display_(Seq[Any](/*39.16*/routes/*39.22*/.Assets.at("bootstrap/css/bootstrap.min.css"))),format.raw/*39.67*/("""" rel="stylesheet" media="screen">		
		<script src=""""),_display_(Seq[Any](/*40.17*/routes/*40.23*/.Assets.at("javascripts/jquery-1.9.1.min.js"))),format.raw/*40.68*/("""" type="text/javascript"></script>
		<script src=""""),_display_(Seq[Any](/*41.17*/routes/*41.23*/.Assets.at("javascripts/script.js"))),format.raw/*41.58*/("""" type="text/javascript"></script>
   </head>
   
   <body>
	
	<!-- Para exibicao de conteudo flash 
		 Por padrão, existem duas areas: success e error
		 Ambas contam com um link capaz de fazer estas div sumirem    --> 
	
	
	"""),_display_(Seq[Any](/*51.3*/if(flash.containsKey("success"))/*51.35*/{_display_(Seq[Any](format.raw/*51.36*/("""
		<div class="alert alert-success">
		<a class="close" data-dismiss="alert">x</a>
		"""),_display_(Seq[Any](/*54.4*/flash/*54.9*/.get("success"))),format.raw/*54.24*/("""
		</div>
	""")))})),format.raw/*56.3*/("""
	"""),_display_(Seq[Any](/*57.3*/if(flash.containsKey("error"))/*57.33*/{_display_(Seq[Any](format.raw/*57.34*/("""
		<div class="alert alert-error">
		<a class="close" data-dismiss="alert">x</a>
		"""),_display_(Seq[Any](/*60.4*/flash/*60.9*/.get("error"))),format.raw/*60.22*/("""
		</div>
	""")))})),format.raw/*62.3*/("""
	<!-- fim conteudp flash -->
	
	
	
	<!-- BARRA TOPO -->	
	"""),_display_(Seq[Any](/*68.3*/components/*68.13*/.topbar())),format.raw/*68.22*/("""	



	<div class="container-fluid">
		<div class="row-fluid ">			
				<!-- BODY CONTENT -->
				<div class="span3 menuLateral">
					<!-- Menu -->	
					"""),_display_(Seq[Any](/*77.7*/if(session.contains("connected"))/*77.40*/ {_display_(Seq[Any](format.raw/*77.42*/("""
						"""),_display_(Seq[Any](/*78.8*/components/*78.18*/.menu())),format.raw/*78.25*/("""
					""")))})),format.raw/*79.7*/("""
				</div>
				<div class="span9 div_categorias">"""),_display_(Seq[Any](/*81.40*/content)),format.raw/*81.47*/("""</div>
				
			</div>
		</div>
	</div>	  

	<!-- RODAPE  -->
	<div class="footer" style="position: absolute; bottom:0;  width:79%;margin-left: 219px;">
		<p>&copy; 2015 <span class="label label-important"> ZINF </span>  <b>Z</b>inf <b>I</b>s <b>N</b>ot <b>F</b>orum</p>
	</div>


</body>

</html>
"""))}
    }
    
    def render(title:String,content:Html): play.api.templates.HtmlFormat.Appendable = apply(title)(content)
    
    def f:((String) => (Html) => play.api.templates.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 15:30:09 BRST 2015
                    SOURCE: C:/Users/anndreyfrancys/Documents/Projetos/zinf/app/views/main.scala.html
                    HASH: 411b2f00ce25367dff00bdb9ef32ac8bc36953a5
                    MATRIX: 778->1|902->31|1088->182|1114->187|1214->251|1229->257|1301->306|1393->362|1408->368|1480->417|1572->473|1587->479|1659->528|1751->584|1766->590|1838->639|1932->697|1947->703|2021->754|2115->812|2130->818|2204->869|2298->927|2313->933|2387->984|2481->1042|2496->1048|2570->1099|2664->1157|2679->1163|2753->1214|2853->1278|2868->1284|2944->1337|3041->1398|3056->1404|3125->1450|3222->1511|3237->1517|3306->1563|3403->1624|3418->1630|3487->1676|4329->2482|4344->2488|4400->2522|4487->2573|4502->2579|4559->2614|4614->2633|4629->2639|4696->2684|4786->2738|4801->2744|4868->2789|4956->2841|4971->2847|5028->2882|5300->3119|5341->3151|5380->3152|5504->3241|5517->3246|5554->3261|5599->3275|5638->3279|5677->3309|5716->3310|5838->3397|5851->3402|5886->3415|5931->3429|6032->3495|6051->3505|6082->3514|6280->3677|6322->3710|6362->3712|6406->3721|6425->3731|6454->3738|6493->3746|6582->3799|6611->3806
                    LINES: 26->1|29->1|36->8|36->8|38->10|38->10|38->10|39->11|39->11|39->11|40->12|40->12|40->12|41->13|41->13|41->13|42->14|42->14|42->14|43->15|43->15|43->15|44->16|44->16|44->16|45->17|45->17|45->17|46->18|46->18|46->18|47->19|47->19|47->19|48->20|48->20|48->20|49->21|49->21|49->21|50->22|50->22|50->22|65->37|65->37|65->37|66->38|66->38|66->38|67->39|67->39|67->39|68->40|68->40|68->40|69->41|69->41|69->41|79->51|79->51|79->51|82->54|82->54|82->54|84->56|85->57|85->57|85->57|88->60|88->60|88->60|90->62|96->68|96->68|96->68|105->77|105->77|105->77|106->78|106->78|106->78|107->79|109->81|109->81
                    -- GENERATED --
                */
            