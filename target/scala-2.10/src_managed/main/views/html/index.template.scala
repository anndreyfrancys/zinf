
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object index extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Form[User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(formUser: Form[User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._


Seq[Any](format.raw/*1.24*/("""
"""),format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*4.2*/main("ZINF")/*4.14*/{_display_(Seq[Any](format.raw/*4.15*/("""
	<script src=""""),_display_(Seq[Any](/*5.16*/routes/*5.22*/.Assets.at("javascripts/main.js"))),format.raw/*5.55*/("""" type="text/javascript"></script>

	"""),_display_(Seq[Any](/*7.3*/if(session.contains("connected"))/*7.36*/ {_display_(Seq[Any](format.raw/*7.38*/("""
		<h2 class= "titleText">Escolha um grupo</h2>

		<div class="breadcrumbs"></div>


		<!--Grupo -->
		<div class="categorias">
			<div class="row-fluid">

			 """),_display_(Seq[Any](/*17.6*/for(Grupo <- Grupo.find.all()) yield /*17.36*/{_display_(Seq[Any](format.raw/*17.37*/("""
				<div class="boxShadow option" id=""""),_display_(Seq[Any](/*18.40*/Grupo/*18.45*/.id_grupo)),format.raw/*18.54*/("""">
					"""),_display_(Seq[Any](/*19.7*/Grupo/*19.12*/.nome_grupo)),format.raw/*19.23*/("""
				</div>
			""")))})),format.raw/*21.5*/("""
			</div>
		</div>
	""")))}/*24.3*/else/*24.7*/{_display_(Seq[Any](format.raw/*24.8*/("""
	<div class="formulario">
			<h2>Realize o seu cadastro</h2>		
			"""),_display_(Seq[Any](/*27.5*/form(routes.Application.cadUser)/*27.37*/ {_display_(Seq[Any](format.raw/*27.39*/("""
				<input class = "input-form" type="text" id="email" name="email" value="" placeholder="Digite o seu Email"><br>
				<input class = "input-form" type="text" id="name" name="name" value="" placeholder="Digite o seu nome"><br>
				<input class = "input-form" type="password" id="password" name="password" placeholder="Digite a sua senha"><br>
				<input type="submit" class="btn btn-success button" value="Cadastrar usuario">
			""")))})),format.raw/*32.5*/("""
		</div>
		

	""")))})),format.raw/*36.3*/("""
	
""")))})),format.raw/*38.2*/("""
"""))}
    }
    
    def render(formUser:Form[User]): play.api.templates.HtmlFormat.Appendable = apply(formUser)
    
    def f:((Form[User]) => play.api.templates.HtmlFormat.Appendable) = (formUser) => apply(formUser)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 15:30:09 BRST 2015
                    SOURCE: C:/Users/anndreyfrancys/Documents/Projetos/zinf/app/views/index.scala.html
                    HASH: d0b7400bcb17aeb9db2176c99172a33e7b722122
                    MATRIX: 778->1|911->23|939->43|976->46|996->58|1034->59|1086->76|1100->82|1154->115|1228->155|1269->188|1308->190|1514->361|1560->391|1599->392|1676->433|1690->438|1721->447|1766->457|1780->462|1813->473|1862->491|1905->516|1917->520|1955->521|2061->592|2102->624|2142->626|2609->1062|2660->1082|2697->1088
                    LINES: 26->1|30->1|31->3|32->4|32->4|32->4|33->5|33->5|33->5|35->7|35->7|35->7|45->17|45->17|45->17|46->18|46->18|46->18|47->19|47->19|47->19|49->21|52->24|52->24|52->24|55->27|55->27|55->27|60->32|64->36|66->38
                    -- GENERATED --
                */
            