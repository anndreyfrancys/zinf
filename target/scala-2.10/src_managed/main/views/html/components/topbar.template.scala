
package views.html.components

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object topbar extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply():play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.2*/("""<!-- BARRA TOPO -->
	
	<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*3.47*/routes/*3.53*/.Assets.at("stylesheets/navbar.css"))),format.raw/*3.89*/("""">
	
	<div class="navbar">
		<div class="navbar-inner"> 
			<a class="brand cursor"><span class="icon-th"></span></a>
			<!-- SPAN PARA LOGIN -->
			 <span class="navbar-text pull-right"> 
				"""),_display_(Seq[Any](/*10.6*/if(session.contains("connected"))/*10.39*/ {_display_(Seq[Any](format.raw/*10.41*/("""
			 		"""),_display_(Seq[Any](/*11.8*/session/*11.15*/.get("connected"))),format.raw/*11.32*/(""" <a href=""""),_display_(Seq[Any](/*11.43*/routes/*11.49*/.Application.logout)),format.raw/*11.68*/(""""> <span class="label lbl-info"> Sair</span> </a>
				 """)))}/*12.7*/else/*12.12*/{_display_(Seq[Any](format.raw/*12.13*/("""
			  		<a  href=""""),_display_(Seq[Any](/*13.19*/routes/*13.25*/.Application.login)),format.raw/*13.43*/(""""> <span class="label lbl-info">Login <span>  </a> 
			 	""")))})),format.raw/*14.7*/("""

			 </span>
			 <!-- FIM SPAN PARA LOGIN -->
		<ul class="nav">
			 <li class="divider-vertical"></li>
			 <li><a class="brand" href="/">ZINF</a> </li>
			"""),_display_(Seq[Any](/*21.5*/if(session.contains("connected"))/*21.38*/ {_display_(Seq[Any](format.raw/*21.40*/("""
				<li><a class="brand" href="/meusgrupos"><img src=""""),_display_(Seq[Any](/*22.56*/routes/*22.62*/.Assets.at("images/ico/mygroups.png"))),format.raw/*22.99*/(""""></a> </li>
				<!--<li><a class="brand" href="/arquivos"><img src=""""),_display_(Seq[Any](/*23.58*/routes/*23.64*/.Assets.at("images/ico/files.png"))),format.raw/*23.98*/(""""></a> </li>-->
				<li><a class="brand" href="/sobre"><img src=""""),_display_(Seq[Any](/*24.51*/routes/*24.57*/.Assets.at("images/ico/about.png"))),format.raw/*24.91*/(""""></a> </li>
				<li><a class="brand" href="/cadGrupo"><img src=""""),_display_(Seq[Any](/*25.54*/routes/*25.60*/.Assets.at("images/ico/category.png"))),format.raw/*25.97*/(""""></a> </li>
			""")))})),format.raw/*26.5*/("""
		</ul>
	</div>
	</div>		
	</p>
	
			 <!-- FIM BARRA TOPO -->"""))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 15:30:09 BRST 2015
                    SOURCE: C:/Users/anndreyfrancys/Documents/Projetos/zinf/app/views/components/topbar.scala.html
                    HASH: 5d48c90c1c8fa11a8f093fcae4777d8195c6a8b2
                    MATRIX: 867->1|972->71|986->77|1043->113|1279->314|1321->347|1361->349|1405->358|1421->365|1460->382|1507->393|1522->399|1563->418|1638->475|1651->480|1690->481|1746->501|1761->507|1801->525|1891->584|2091->749|2133->782|2173->784|2266->841|2281->847|2340->884|2447->955|2462->961|2518->995|2621->1062|2636->1068|2692->1102|2795->1169|2810->1175|2869->1212|2918->1230
                    LINES: 29->1|31->3|31->3|31->3|38->10|38->10|38->10|39->11|39->11|39->11|39->11|39->11|39->11|40->12|40->12|40->12|41->13|41->13|41->13|42->14|49->21|49->21|49->21|50->22|50->22|50->22|51->23|51->23|51->23|52->24|52->24|52->24|53->25|53->25|53->25|54->26
                    -- GENERATED --
                */
            