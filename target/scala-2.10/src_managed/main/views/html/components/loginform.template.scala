
package views.html.components

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object loginform extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Form[Application.Login],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(formLogin: Form[Application.Login]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.38*/("""


"""),_display_(Seq[Any](/*4.2*/helper/*4.8*/.form(routes.Application.authenticate)/*4.46*/ {_display_(Seq[Any](format.raw/*4.48*/("""
	<script src=""""),_display_(Seq[Any](/*5.16*/routes/*5.22*/.Assets.at("javascripts/login.js"))),format.raw/*5.56*/("""" type="text/javascript"></script>
	<div class="formulario">
		<h1>Login</h1>

		"""),_display_(Seq[Any](/*9.4*/if(formLogin.hasGlobalErrors)/*9.33*/ {_display_(Seq[Any](format.raw/*9.35*/(""" 
			<p class="error">
			"""),_display_(Seq[Any](/*11.5*/formLogin/*11.14*/.globalError.message)),format.raw/*11.34*/("""
			</p>
		""")))})),format.raw/*13.4*/("""

		"""),_display_(Seq[Any](/*15.4*/if(flash.contains("success"))/*15.33*/ {_display_(Seq[Any](format.raw/*15.35*/("""
			<p class="success">
			"""),_display_(Seq[Any](/*17.5*/flash/*17.10*/.get("success"))),format.raw/*17.25*/("""
			</p>
		""")))})),format.raw/*19.4*/("""

	
		<input class = "input-form" type="email" name="email" placeholder="Email" value=""""),_display_(Seq[Any](/*22.85*/formLogin("email")/*22.103*/.value)),format.raw/*22.109*/("""">
		<input class = "input-form" type="password" name="password" placeholder="Password">
		<input type="submit" value="Login" class="btn btn-success button">
	<div>
""")))})),format.raw/*26.2*/("""

"""))}
    }
    
    def render(formLogin:Form[Application.Login]): play.api.templates.HtmlFormat.Appendable = apply(formLogin)
    
    def f:((Form[Application.Login]) => play.api.templates.HtmlFormat.Appendable) = (formLogin) => apply(formLogin)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 15:30:09 BRST 2015
                    SOURCE: C:/Users/anndreyfrancys/Documents/Projetos/zinf/app/views/components/loginform.scala.html
                    HASH: 420e1bca411227c6f42d52e5b9bda58812b3dc09
                    MATRIX: 806->1|936->37|977->44|990->50|1036->88|1075->90|1127->107|1141->113|1196->147|1316->233|1353->262|1392->264|1456->293|1474->302|1516->322|1561->336|1603->343|1641->372|1681->374|1746->404|1760->409|1797->424|1842->438|1969->529|1997->547|2026->553|2227->723
                    LINES: 26->1|29->1|32->4|32->4|32->4|32->4|33->5|33->5|33->5|37->9|37->9|37->9|39->11|39->11|39->11|41->13|43->15|43->15|43->15|45->17|45->17|45->17|47->19|50->22|50->22|50->22|54->26
                    -- GENERATED --
                */
            