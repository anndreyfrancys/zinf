
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object notFound extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(requestPath: String):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import helper.twitterBootstrap._


Seq[Any](format.raw/*1.23*/("""
"""),_display_(Seq[Any](/*4.2*/main("404: Not found")/*4.24*/ {_display_(Seq[Any](format.raw/*4.26*/("""
  <h2>404: Não encontrado</h2>
  <p>Infelizmente, não posso processar seu pedido ["""),_display_(Seq[Any](/*6.53*/requestPath)),format.raw/*6.64*/("""]</p>
  
  <a href=""""),_display_(Seq[Any](/*8.13*/routes/*8.19*/.Application.index())),format.raw/*8.39*/(""""><span class="btn btn-success"> Voltar para a página inicial </span></a>
  
  
""")))})))}
    }
    
    def render(requestPath:String): play.api.templates.HtmlFormat.Appendable = apply(requestPath)
    
    def f:((String) => play.api.templates.HtmlFormat.Appendable) = (requestPath) => apply(requestPath)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 15:30:09 BRST 2015
                    SOURCE: C:/Users/anndreyfrancys/Documents/Projetos/zinf/app/views/notFound.scala.html
                    HASH: d26a0102fa9b62ba5ff1a16fdc90dbd8dd21d397
                    MATRIX: 777->1|944->22|981->78|1011->100|1050->102|1171->188|1203->199|1261->222|1275->228|1316->248
                    LINES: 26->1|32->1|33->4|33->4|33->4|35->6|35->6|37->8|37->8|37->8
                    -- GENERATED --
                */
            