
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object cadGrupo extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Form[Grupo],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(formGrupo: Form[Grupo]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._


Seq[Any](format.raw/*1.26*/("""
"""),format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*4.2*/main("ZINF - Cadastrar Grupos")/*4.33*/ {_display_(Seq[Any](format.raw/*4.35*/("""  
	<h1>Cadastrar Grupo</h1>   

	<div class="left">
		<h4>Cadastrar</h4>
	
		<div class="formGrupo">
			<form action="/addGrupo" method="POST">
				<input class = "input-form" type="text" id="nome_grupo" name="nome_grupo" value="" placeholder="Digite o nome do grupo">  	
				<input class="button btn btn-success" type="submit"value="Cadastrar grupo">			
			</form>
			
		</div>
	</div> 
	<div class="right">
		<h4>Grupos cadastrados</h4>
		<ul>
			"""),_display_(Seq[Any](/*21.5*/for(Grupo <- Grupo.find.all()) yield /*21.35*/{_display_(Seq[Any](format.raw/*21.36*/("""
				<li>
					"""),_display_(Seq[Any](/*23.7*/Grupo/*23.12*/.nome_grupo)),format.raw/*23.23*/(""" 
					<span class="icon-edit"></span> <a href='"""),_display_(Seq[Any](/*24.48*/routes/*24.54*/.Application.editaGrupo(Grupo.id_grupo))),format.raw/*24.93*/("""'> editar</a>
					<a href='"""),_display_(Seq[Any](/*25.16*/routes/*25.22*/.Application.deletaGrupo(Grupo.id_grupo))),format.raw/*25.62*/("""'> Deletar</a>
				</li>
			""")))})),format.raw/*27.5*/("""  
		</ul>
	</div> 
	
	



	
	
""")))})))}
    }
    
    def render(formGrupo:Form[Grupo]): play.api.templates.HtmlFormat.Appendable = apply(formGrupo)
    
    def f:((Form[Grupo]) => play.api.templates.HtmlFormat.Appendable) = (formGrupo) => apply(formGrupo)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 15:30:09 BRST 2015
                    SOURCE: C:/Users/anndreyfrancys/Documents/Projetos/zinf/app/views/cadGrupo.scala.html
                    HASH: ac7687b1e9e462c65e5173598756e7a897cae95f
                    MATRIX: 782->1|917->25|945->45|982->48|1021->79|1060->81|1563->549|1609->579|1648->580|1701->598|1715->603|1748->614|1834->664|1849->670|1910->709|1976->739|1991->745|2053->785|2115->816
                    LINES: 26->1|30->1|31->3|32->4|32->4|32->4|49->21|49->21|49->21|51->23|51->23|51->23|52->24|52->24|52->24|53->25|53->25|53->25|55->27
                    -- GENERATED --
                */
            