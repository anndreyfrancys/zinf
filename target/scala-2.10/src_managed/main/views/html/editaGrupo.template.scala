
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object editaGrupo extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[Grupo],Grupo,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(formGrupo: Form[Grupo], grupo: Grupo):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._


Seq[Any](format.raw/*1.40*/("""
"""),format.raw/*3.1*/("""

"""),_display_(Seq[Any](/*5.2*/main("Editar grupo")/*5.22*/{_display_(Seq[Any](format.raw/*5.23*/("""
	<div class="formulario">
		"""),_display_(Seq[Any](/*7.4*/form(routes.Application.updateGrupo(grupo.id_grupo))/*7.56*/{_display_(Seq[Any](format.raw/*7.57*/("""
			"""),_display_(Seq[Any](/*8.5*/inputText(formGrupo("nome_grupo")))),format.raw/*8.39*/("""

			<input type="submit" class="btn btn-success button" value="Alterar">

		""")))})),format.raw/*12.4*/("""
	</div>
""")))})))}
    }
    
    def render(formGrupo:Form[Grupo],grupo:Grupo): play.api.templates.HtmlFormat.Appendable = apply(formGrupo,grupo)
    
    def f:((Form[Grupo],Grupo) => play.api.templates.HtmlFormat.Appendable) = (formGrupo,grupo) => apply(formGrupo,grupo)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri Nov 13 15:30:09 BRST 2015
                    SOURCE: C:/Users/anndreyfrancys/Documents/Projetos/zinf/app/views/editaGrupo.scala.html
                    HASH: 687955a033a1a2fc557813b00fe1c2f166b8880a
                    MATRIX: 790->1|939->39|967->59|1006->64|1034->84|1072->85|1138->117|1198->169|1236->170|1276->176|1331->210|1444->292
                    LINES: 26->1|30->1|31->3|33->5|33->5|33->5|35->7|35->7|35->7|36->8|36->8|40->12
                    -- GENERATED --
                */
            