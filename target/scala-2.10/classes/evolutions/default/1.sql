# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table grupo (
  id_grupo                  bigint not null,
  nome_grupo                varchar(255),
  constraint pk_grupo primary key (id_grupo))
;

create table account (
  email                     varchar(255) not null,
  name                      varchar(255),
  password                  varchar(255),
  constraint pk_account primary key (email))
;

create sequence grupo_seq;

create sequence account_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists grupo;

drop table if exists account;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists grupo_seq;

drop sequence if exists account_seq;

