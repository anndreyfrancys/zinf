$(document).ready(function(){

	$(".brand:eq(0)").click(hideMenuLateral);
	//Oculta o menu lateral
	function hideMenuLateral(){
		if(loadLocal("menuLateral") != "hide"){
			$(".menuLateral").hide("slow", function(){
				$(".div_categorias").removeClass("span9").addClass('span12');				
			});
			loadLocal("menuLateral","hide");
		}else{
			$(".menuLateral").show("slow");
				$(".div_categorias").removeClass("span12").addClass('span9');				
			loadLocal("menuLateral","show");
		}
	}

	//Verifica se no local storage já não existia o parametro hide para o menu lateral
	if(loadLocal("menuLateral") == "hide" ) {
		$(".menuLateral").hide()
		$(".div_categorias").removeClass("span9").addClass('span12');				
	}


	//Função usada para verificar se existe a variavel e retornaro valor da varaivel do localstorage
	function loadLocal(variavel, value){
		// Torna a variavel opcional
		value = typeof value !== 'undefined' ? value : '';

		// verifica se a variavel exite no localstorage e se o valor de 
		if(localStorage.getItem(variavel)){
			if(value != "")
				localStorage.setItem(variavel, value);

			return localStorage.getItem(variavel);
			
		}else{
			localStorage.setItem(variavel, value);
			return localStorage.getItem(variavel);
		}

	}

	$(".alert").click(function(){
		$(this).remove();
	});




})