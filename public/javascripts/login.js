$(document).ready(function(){
	//Função para pegar as variaveis via get
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		    results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	$("input[name=email]").val(getParameterByName('user'));

	if(getParameterByName('user') != "")
		$("input[name=password]").focus();
	else
		$("input[name=email]").focus();

})