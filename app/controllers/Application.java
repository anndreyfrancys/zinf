package controllers;

import models.User;

import com.avaje.ebean.Ebean;
import com.sun.javafx.tk.Toolkit.Task;

import models.Grupo;
import play.*;
import play.data.Form;
import play.mvc.*;
import play.Logger;

import views.html.*;

public class Application extends Controller {

	//formulario para fazer login
	static Form<Login> formLogin = Form.form(Login.class);

	//Formulário cadastro de categoria
	static Form<Grupo> formGrupo = Form.form(Grupo.class);

	//Cadastro de usuário
	static Form<User> formUser = Form.form(User.class);


	/*
	 * ACTIONS DO CONTROLLERS
	 */

	public static Result index() {
		return ok(index.render(formUser));
	}

	public static Result sobre(){
		return ok(sobre.render("Sobre"));
	}

	public static Result arquivos(){
		return ok(arquivos.render("Arquivos"));
	}

	// public static Result grupo(){
	// 	return ok(grupo.render("Grupo"));
	// }

	public static Result meusgrupos(){
		return ok(meusgrupos.render("Grupo"));
	}

	public static Result cadGrupo(){
		return ok(cadGrupo.render(formGrupo));
	}

	public static Result addGrupo(){
		Form<Grupo> formulario = formGrupo.bindFromRequest();
		Grupo categoria = formulario.get();
		categoria.save();
		flash("success", "Grupo cadastrada com sucesso");
		return redirect("/cadGrupo");
	}

	// public static Result addGrupo(){
	// 	Form<Grupo> formulario = formGrupo.bindFromRequest();
	// 	Grupo categoria = formulario.get();
	// 	if(categoria.nome_grupo != ""){
	// 		categoria.save();
	// 		flash("success", "Grupo cadastrada com sucesso");
	// 	}else{
	// 		flash("error", "Nome do grupo não pode ficar em branco.");
	// 	}	
	// 	return redirect("/cadGrupo");
	// }

	public static Result cadUser(){
		Form<User> formulario = formUser.bindFromRequest();	
		User user = formulario.get();
		// Logger.info(formulario.toString());
		// Logger.info(user.password.toString());
		user.password = BCrypt.hashpw(user.password, BCrypt.gensalt());
		// user.password = passwordHash;
		user.save();
//		user.update(password, passwordHashs);
		flash("success", "Cadastro realizado com sucesso");
		return redirect("/login?user=" + user.email);
	}
	
	public static Result deletaGrupo(Long id) {
	    Ebean.delete(Ebean.find(Grupo.class,id));
	    flash("success", "Removido com sucesso.");
		return redirect(routes.Application.cadGrupo());
	}

	 //Função para editar do grupo
	public static Result editaGrupo(Long id){

		Grupo grupoEscolhido = Grupo.findL.byId(id);
		Form <Grupo> form = formGrupo.fill(grupoEscolhido);
		return ok(editaGrupo.render(form, grupoEscolhido));

	}

	//Função para fazer update no grupo
	public static Result updateGrupo(Long id){
		Form<Grupo> formAlterar = formGrupo.bindFromRequest();
		formAlterar.get().update(id);
		flash("success", "Grupo alterado com sucesso");
		return redirect("/cadGrupo");

	}

	//Função para editar do grupo
	public static Result readGrupo(Long id){

		Grupo grupoEscolhido = Grupo.findL.byId(id);
		return ok(grupo.render(grupoEscolhido));

	}




	/*
	 * METODOS E CLASSES PARA LOGIN
	 */
	// -- Classe para autenticacao
	public static class Login {

		// dados necessarios para login
		public String email;
		public String password;

		// AQUI ESTA O TRUQUE, COMO VALIDAR UM FORMULARIO
		// NA HORA EM QUE O FORMULARIO É VALIDADO, EXECUTAMOS O LOGIN!
		// O metodo validade é invocado quando o framework play valida o
		// formulario de login
		//Observe que ele invoca o metodo authenticate do model User
		//e nao o authenticate que encontra-se neste controller
		public String validate() {
			if (User.authenticate(email, password) == null) {
				return "Usuario ou senha inválidos";
			}
			return null;
		}

	} // -- fim da classe login

	/**
	 * Login page.
	 */
	public static Result login() {
		return ok(login.render(formLogin));
	}

	/**
	 * Atua na verificacao do formulario do login
	 */
	public static Result authenticate() {
		
		//recebe formulario de login
		Form<Login> loginForm = formLogin.bindFromRequest();
		//valida o formulario e, ao mesmo tempo, se o usuario conseguiu se autenticar
		if (loginForm.hasErrors()) {
			flash("error", "Login ou senha inválida. Tente novamente");
			//caso nao, envia novamente para o usuario
			return badRequest(login.render(loginForm));
		} else {
			//usuario autenticado
			//posso inserir dados na sessão
	
			session("connected", loginForm.get().email);
			
			//rediciono ele para a pagina inicial
			return redirect(routes.Application.index());
		}
	}

	/**
	 * Logout and clean the session.
	 */
	public static Result logout() {
		session().clear();
		flash("success", "Sessão finalizada com sucesso.");
		return redirect(routes.Application.index());
	}


}
