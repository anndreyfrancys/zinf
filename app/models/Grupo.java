package models;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class Grupo extends Model{
	
	@Id
	public Long id_grupo;
	@Required
	public String nome_grupo;	
	
     public static Model.Finder<String,Grupo> find = new Model.Finder(String.class, Grupo.class);

     public static Model.Finder<Long,Grupo> findL = new Model.Finder(Long.class, Grupo.class);

   
}